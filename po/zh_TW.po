# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the Authenticator package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Authenticator\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-20 05:14+0200\n"
"PO-Revision-Date: 2019-01-29 03:08+0000\n"
"Last-Translator: Louies <louies0623@gmail.com>\n"
"Language-Team: Chinese (Traditional) <https://hosted.weblate.org/projects/"
"authenticator/translation/zh_Hant/>\n"
"Language: zh_Hant\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 3.5-dev\n"

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:6
#: data/com.github.bilelmoussaoui.Authenticator.desktop.in.in:3
#: src/Authenticator/application.py.in:37
msgid "Authenticator"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:7
#: data/com.github.bilelmoussaoui.Authenticator.desktop.in.in:5
msgid "Two-factor authentication code generator"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:9
msgid ""
"Simple application that generates a two-factor authentication code, created "
"for GNOME."
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:10
msgid "Features:"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:12
msgid "QR code scanner"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:13
msgid "Beautiful UI"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:14
msgid "Huge database of more than 560 supported services"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:15
#, fuzzy
msgid "Keep your PIN tokens secure by locking the application with a password"
msgstr "鎖定應用程式"

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:16
msgid "Automtically fetch an image for services using their favicon"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:17
msgid "The possibility to add new services"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:37
msgid ""
"New 3.31 beta is out! Here's an overview of what changed Please backup your "
"accounts before updating to the latest version and restore the data after "
"update."
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:42
msgid "New settings window"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:43
msgid "Download provider images using their favicon if possible"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:44
msgid "Night Light feature: not working yet!"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:45
msgid ""
"The possiblity to enable/disable a password without having to reset "
"everything"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:46
msgid ""
"You can now add new providers and set images for providers that we didn't "
"find an icon for"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:47
msgid "Mobile ready"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:48
msgid "New icon by Tobias"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:49
msgid "Bunch of fixed bugs"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:55
msgid ""
"Since I have moved the application to GOME Gitlab's World group, a Flatpak "
"build for each new commit is available to download from the site's website."
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:59
msgid "Backup and restore from a basic JSON file"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:60
msgid "Backup and restore from an encrypted-GPG JSON file"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:61
msgid "Add andOTP support (free and open Android 2FA application)"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:62
msgid "New Settings Widow with 3 sections: appearance, behavior and backup"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:63
msgid "Fix Flatpak Build with the latest GNOME Runtime"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:64
msgid "Move the project to GOME Gitlab's World group"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:71
msgid "GNOME Shell Search provider"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:72
msgid "Codes expire simultaneously #91"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:79
msgid "Revamped main window to more closely follow the GNOME HIG"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:80
msgid "Revamped add a new account window to make it easier to use"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:81
msgid ""
"Possibility to add an account from a provider not listed in the shipped "
"database"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:82
msgid "Possibilty to edit an account"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:83
msgid "One Time Password now visible by default"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:90
msgid "Fix python-dbus by using GDbus instead"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:97
msgid "Fix the QRScanner on GNOME Shell"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:98
msgid "Add a new entry for the account's username"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:99
msgid "Updated database of supported accounts"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:106
msgid "HOTFIX: App not running in DE other than GNOME"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:113
msgid "Rename project to Authenticator"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:114
msgid "Cleaner code base"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:115
msgid "Faster startup"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:116
msgid "Remove unneeded features"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:117
msgid "Switch to pyzbar instead of zbarlight"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:118
msgid "Flatpak package"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.appdata.xml.in.in:134
msgid "Bilal Elmoussaoui"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.desktop.in.in:4
msgid "Two-factor authentication"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.desktop.in.in:10
msgid "Gnome;GTK;Verification;"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.desktop.in.in:11
msgid "@appid@"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.gschema.xml.in:6
#: data/com.github.bilelmoussaoui.Authenticator.gschema.xml.in:7
msgid "Default window position"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.gschema.xml.in:11
#: src/Authenticator/widgets/settings.py:83
#, fuzzy
msgid "Dark Theme"
msgstr "黑暗主題"

#: data/com.github.bilelmoussaoui.Authenticator.gschema.xml.in:12
#: src/Authenticator/widgets/settings.py:84
msgid "Whether the application should use a dark theme."
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.gschema.xml.in:16
#: src/Authenticator/widgets/settings.py:88
msgid "Night Light"
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.gschema.xml.in:17
#: src/Authenticator/widgets/settings.py:89
msgid "Automatically enable dark mode at night."
msgstr ""

#: data/com.github.bilelmoussaoui.Authenticator.gschema.xml.in:21
msgid "Default window maximized behaviour"
msgstr ""

#: data/ui/about_dialog.ui.in:10
msgid "Two-factor authentication code generator."
msgstr ""

#: data/ui/about_dialog.ui.in:13
msgid "translator-credits"
msgstr ""

#: data/ui/account_add.ui:22
msgid "Add a new account"
msgstr ""

#: data/ui/account_add.ui:25 data/ui/account_edit.ui:19
msgid "Close"
msgstr "關閉"

#: data/ui/account_add.ui:34
msgid "Add"
msgstr "添加"

#: data/ui/account_add.ui:54
#, fuzzy
msgid "Scan QR Code"
msgstr "掃描 QR Code"

#: data/ui/account_config.ui:85
msgid "Enable 2FA for this account"
msgstr ""

#: data/ui/account_config.ui:86
msgid "2FA Token"
msgstr ""

#: data/ui/account_config.ui:100
msgid "Account Name"
msgstr ""

#: data/ui/account_config.ui:123
msgid "Provider"
msgstr "供應商"

#: data/ui/account_edit.ui:28 data/ui/settings.ui.in:50
msgid "Save"
msgstr "儲存"

#: data/ui/account_row.ui:23
msgid "Edit"
msgstr "編輯"

#: data/ui/account_row.ui:41
msgid "Delete"
msgstr "删除"

#: data/ui/account_row.ui:97
msgid "Copy PIN to clipboard"
msgstr ""

#: data/ui/password_widget.ui:46
#, fuzzy
msgid "Current Password:"
msgstr "再次輸入密碼"

#: data/ui/password_widget.ui:90
#, fuzzy
msgid "New Password:"
msgstr "密碼"

#: data/ui/password_widget.ui:135
msgid "Confirm New Password:"
msgstr ""

#: data/ui/password_widget.ui:183 src/Authenticator/widgets/settings.py:208
#, fuzzy
msgid "Change Password"
msgstr "密碼"

#: data/ui/password_widget.ui:204
#, fuzzy
msgid "Delete Password"
msgstr "再次輸入密碼"

#: data/ui/provider_image.ui:41
msgid "We couldn't find an image for this provider"
msgstr ""

#. Night mode action
#: data/ui/settings.ui.in:28 src/Authenticator/application.py.in:112
msgid "Preferences"
msgstr "偏好設定"

#: data/ui/settings.ui.in:95
msgid "Behaviour"
msgstr ""

#: data/ui/shortcuts.ui:13
msgctxt "shortcut window"
msgid "General"
msgstr ""

#: data/ui/shortcuts.ui:17
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr ""

#: data/ui/shortcuts.ui:24
msgctxt "shortcut window"
msgid "Preferences"
msgstr ""

#: data/ui/shortcuts.ui:31
msgctxt "shortcut window"
msgid "Quit"
msgstr ""

#: data/ui/shortcuts.ui:40
msgctxt "shortcut window"
msgid "Accounts"
msgstr ""

#: data/ui/shortcuts.ui:44
msgctxt "shortcut window"
msgid "Add"
msgstr ""

#: data/ui/shortcuts.ui:51
msgctxt "shortcut window"
msgid "Search"
msgstr ""

#: data/ui/window.ui.in:182
msgid "There are no accounts yet…"
msgstr ""

#: data/ui/window.ui.in:211
msgid "Add a new account from the menu"
msgstr ""

#: data/ui/window.ui.in:227
#, fuzzy
msgid "Scan a QR Code"
msgstr "掃描 QR Code"

#: data/ui/window.ui.in:373
msgid "No results found"
msgstr ""

#: data/ui/window.ui.in:437
msgid "Authenticator is locked"
msgstr ""

#: data/ui/window.ui.in:476
msgid "Unlock"
msgstr ""

#: src/authenticator.py.in:50
msgid "Start in debug mode"
msgstr ""

#: src/authenticator.py.in:52
msgid "Authenticator version number"
msgstr ""

#: src/Authenticator/application.py.in:92
#: src/Authenticator/widgets/settings.py:92
msgid "Lock the application"
msgstr "鎖定應用程式"

#: src/Authenticator/application.py.in:100
msgid "from a plain-text JSON file"
msgstr ""

#: src/Authenticator/application.py.in:101
msgid "in a plain-text JSON file"
msgstr ""

#: src/Authenticator/application.py.in:103
msgid "Restore"
msgstr "還原"

#: src/Authenticator/application.py.in:104
msgid "Backup"
msgstr "備份"

#: src/Authenticator/application.py.in:113
msgid "Donate"
msgstr "贊助"

#: src/Authenticator/application.py.in:114
msgid "Keyboard Shortcuts"
msgstr "鍵盤快速鍵"

#: src/Authenticator/application.py.in:115
msgid "About Authenticator"
msgstr ""

#: src/Authenticator/models/account.py:71
msgid "Default"
msgstr ""

#: src/Authenticator/widgets/accounts/add.py:58
msgid "Invalid QR code"
msgstr "無效的 QR code"

#: src/Authenticator/widgets/accounts/list.py:143
msgid "The One-Time Passwords expires in {} seconds"
msgstr ""

#: src/Authenticator/widgets/accounts/row.py:80
msgid "Couldn't generate the secret code"
msgstr ""

#: src/Authenticator/widgets/accounts/row.py:91
msgid "The PIN of {} was copied to the clipboard"
msgstr ""

#: src/Authenticator/widgets/accounts/row.py:123
msgid "The account was updated successfully"
msgstr ""

#: src/Authenticator/widgets/notification.py:43
#, fuzzy
msgid "Close the notification"
msgstr "鎖定應用程式"

#: src/Authenticator/widgets/notification.py:48
#: src/Authenticator/widgets/notification.py:63
msgid "Undo"
msgstr ""

#: src/Authenticator/widgets/settings.py:93
#, fuzzy
msgid "Lock the application with a password"
msgstr "鎖定應用程式"

#: src/Authenticator/widgets/settings.py:157
#, fuzzy
msgid "Authentication password is now enabled."
msgstr "再次輸入密碼"

#: src/Authenticator/widgets/settings.py:159
#, fuzzy
msgid "The authentication password was updated."
msgstr "再次輸入密碼"

#: src/Authenticator/widgets/settings.py:163
#, fuzzy
msgid "The authentication password was deleted."
msgstr "再次輸入密碼"

#: src/Authenticator/widgets/settings.py:204
#, fuzzy
msgid "Save Password"
msgstr "密碼"

#: src/Authenticator/widgets/utils.py:40 src/Authenticator/widgets/utils.py:45
msgid "JSON files"
msgstr ""

#~ msgid "Settings"
#~ msgstr "設定"

#~ msgid "Cancel"
#~ msgstr "取消"

#~ msgid "Copy"
#~ msgstr "複製"

#~ msgid "Search"
#~ msgstr "搜尋"

#~ msgid "Selection mode"
#~ msgstr "選擇模式"

#~ msgid "Appearance"
#~ msgstr "外觀"
